## Documentation

This repository contains the code for the MLOps 3.0 course.
Documntation is done using [Quarto](https://quarto.org) and can be found in GitLab Pages:

- [Read documentation](https://mlops-3-0-mlops-2024-4f6e9b1cdcf8ba68c8e74d5c62538edd6ec7c49d6e.gitlab.io/)

## Project setup

### Workflow management

- [Snakemake](https://snakemake.github.io) is used for workflow management. See [Snakefile](Snakefile) for the workflow definition.

### Configuration management

- [Hydra](https://hydra.cc) is used for configuration management. See [conf](src/iris_hydra/conf/config.yaml) for the configuration.

### Requirements

- Python 3.10 or higher
- [PDM](https://pdm-project.org) - Python Development Manager
- Conda/Mamba for specific dependencies defined in [environment.yml](environment.yml)

### Running with PDM

```bash
# Install dependencies
pdm install --prod

# Run the application
pdm run start
```

### Running with Docker Compose

```bash
docker compose up
```

## Contributing

Refer to [CONTRIBUTING.md](CONTRIBUTING.md) for more information on how to work with the project.
