# dummy test lakefs pipeline

LAKEFS_REPO="lakefs://dummy"
LAKEFS_BRANCH="main"
LAKEFS_V2_BRANCH="v2"

rule all:
  input:
    expand("output/processed_{version}.txt", version=["v1", "v2"])

rule upload_version1:
    output:
        touch("output/data_v1.done")
    shell:
      """
      set -x
      echo "version: 1 $(date +%s)" > output/data.txt
      lakectl fs upload {LAKEFS_REPO}/{LAKEFS_BRANCH}/data.txt -s output/data.txt
      COMMIT_OUTPUT=$(lakectl commit {LAKEFS_REPO}/{LAKEFS_BRANCH} -m "Version 1")

      COMMIT_ID=$(echo "$COMMIT_OUTPUT" | grep 'ID:' | awk '{{print $2}}')
      if [[ -z "$COMMIT_ID" ]]; then
          echo "Commit ID was not found."
          exit 1
      fi

      lakectl tag create {LAKEFS_REPO}/v1 {LAKEFS_REPO}/$COMMIT_ID
      """

rule upload_version2:
    input:
        rules.upload_version1.output
    output:
        touch("output/data_v2.done")
    shell:
      """
      echo "version: 2 $(date +%s)" > output/data.txt
      lakectl fs upload {LAKEFS_REPO}/{LAKEFS_BRANCH}/data.txt -s output/data.txt
      COMMIT_OUTPUT=$(lakectl commit {LAKEFS_REPO}/{LAKEFS_BRANCH} -m "Version 1")

      COMMIT_ID=$(echo "$COMMIT_OUTPUT" | grep 'ID:' | awk '{{print $2}}')
      if [[ -z "$COMMIT_ID" ]]; then
          echo "Commit ID was not found."
          exit 1
      fi

      echo "COMMIT_ID: $COMMIT_ID"
      lakectl tag create {LAKEFS_REPO}/v2 {LAKEFS_REPO}/$COMMIT_ID
      """

rule process_version1:
    input:
        rules.upload_version1.output,
    output:
        "output/processed_v1.txt"
    shell:
        """
        lakectl fs cat {LAKEFS_REPO}/v1/data.txt > {output}
        cat {output} | awk '{{print $0, "processed"}}' > temp_v1.txt
        mv temp_v1.txt {output}
        """

rule process_version2:
    input:
        rules.upload_version2.output,
    output:
        "output/processed_v2.txt"
    shell:
        """
        lakectl fs cat {LAKEFS_REPO}/v2/data.txt > {output}
        cat {output} | awk '{{print $0, "processed"}}' > temp_v2.txt
        mv temp_v2.txt {output}
        """
