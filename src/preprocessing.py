"""Preprocessing Amazon reviews data."""

import argparse
import logging
import re

import nltk
import polars as pl
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

nltk.download("stopwords", quiet=True)
nltk.download("wordnet", quiet=True)


def clean_text(input_text: str) -> str:
    """Preprocess text data.

    Args:
        input_text (str): Text data to be preprocessed.

    Returns:
        str: Preprocessed text data.
    """
    text = input_text.lower()
    text = re.sub(r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*", "", text)
    text = re.sub(r"[0-9 \-_]+", " ", text)  # remove numbers
    text = re.sub(r"[^a-zA-Z]+", " ", text)  # remove special characters
    stop_words = set(stopwords.words("english"))
    text = " ".join([word for word in text.split() if word not in stop_words])
    return text.strip()


def clean_text_updated(input_text: str) -> str:
    """Preprocess text data.

    Args:
        input_text (str): Text data to be preprocessed.

    Returns:
        str: Preprocessed text data.
    """
    text = input_text.lower()

    # Remove URLs, digits, and special characters in one go
    text = re.sub(r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\d+", "", text)

    stop_words = set(stopwords.words("english"))
    text = " ".join([word for word in text.split() if word not in stop_words])

    return re.sub(r"\s+", " ", text).strip()


def lemmatize(input_frame: pl.DataFrame) -> pl.DataFrame:
    """Lemmatize text data.

    Args:
        input_frame (pl.DataFrame): input data frame.

    Returns:
        pl.DataFrame: Data frame with lemmatized text data.
    """
    lemmatizer = WordNetLemmatizer()

    return input_frame.with_columns(
        pl.col("corpus").map_elements(
            lambda input_list: [lemmatizer.lemmatize(token) for token in input_list], return_dtype=pl.List(pl.Utf8)
        )
    )


def save_preprocessed_data(data: pl.DataFrame, output_path: str) -> None:
    """Save preprocessed data to parquet file.

    Args:
        data (pl.DataFrame): Preprocessed data.
        output_path (str): Path to save the preprocessed data.
    """
    log.info(f"Saving preprocessed data to {output_path}")
    data.write_parquet(output_path)


def main() -> None:
    """Main function to preprocess Amazon reviews data."""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--input_path", type=str, default="data/raw/train.csv")
    arg_parser.add_argument("--n_rows", type=int, default=10_000)
    arg_parser.add_argument("--output_path", type=str, default="data/processed/train.parquet")
    args = arg_parser.parse_args()

    df = pl.read_csv(args.input_path, n_rows=args.n_rows)
    # Set column names
    df.columns = ["Polarity", "Title", "Review"]

    # Preprocess text data: clean text, remove stopwords, and lemmatize
    df = df.with_columns(
        pl.col("Review").map_elements(function=clean_text, return_dtype=pl.Utf8).str.split(" ").alias("corpus")
    )
    df = lemmatize(df)

    # Save preprocessed data to parquet file
    save_preprocessed_data(df, args.output_path)


if __name__ == "__main__":
    main()
