### Git workflow

This project uses [GitHub Flow](https://docs.github.com/en/get-started/using-github/github-flow) as a branching model. The `main` branch is protected, and changes are made through pull requests.

- **Branch naming convention**: `feat/feature-name`, `bugfix/bug-name`, `hotfix/hotfix-name`

- **Commit message convention**: conventional commits, e.g. `feat: add new feature`, `fix: resolve bug`. Refer to [conventionalcommits.org](https://www.conventionalcommits.org) for more information.

### Development environment setup

This project uses [PDM](https://pdm-project.org) as a package manager. To set up the development environment, run the following command:

```bash
# Install dependencies
pdm install
```

### Git hooks

This project uses [pre-commit](https://pre-commit.com) to manage git hooks defined in [.pre-commit-config.yaml](.pre-commit-config.yaml).

### Linters and formatters

For linting and formatting, this project uses [ruff](https://github.com/astral-sh/ruff).

Linting and formatting can be done using the following commands:
```bash
# Run ruff linter
ruff check --fix

# Run ruff formatter
ruff format
```
