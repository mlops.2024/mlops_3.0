"""Main module (dummy code)."""

import os


def add(a: int, b: int) -> int:
    """Add two numbers.

    Args:
        a (int): First number.
        b (int): Second number.

    Returns:
        int: Sum of a and b.
    """
    return a + b


def main() -> None:
    """Main function.

    Returns:
        None
    """
    print("Current working directory: ", os.getcwd())
    print("Addition of 2 and 3: ", add(2, 3))


if __name__ == "__main__":
    main()
