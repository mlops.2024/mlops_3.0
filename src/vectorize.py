"""Vectorize Amazon reviews data using TfidfVectorizer."""

import argparse
import pickle

import polars as pl
from scipy.sparse import csr_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split

random_state = 42
vectorizer_params = {"max_features": 10000, "analyzer": "word"}


def load_parquet_data(data_path: str) -> pl.DataFrame:
    """Load data from parquet file.

    Args:
        data_path (str): Path to parquet file.

    Returns:
        pl.DataFrame: Data frame loaded from parquet file.
    """
    return pl.read_parquet(data_path)


def get_train_test_data(df: pl.DataFrame, test_size: float) -> tuple[pl.DataFrame, pl.DataFrame]:
    """Split data into train and test sets.

    Args:
        df (pl.DataFrame): Input data frame.
        test_size (float): Fraction of the data to be used as test set.

    Returns:
        tuple[pl.DataFrame, pl.DataFrame]: Train and test data frames.
    """
    train_df, test_df = train_test_split(df.to_pandas(), test_size=test_size, shuffle=True, random_state=random_state)
    return pl.from_pandas(train_df), pl.from_pandas(test_df)


def train_vectorizer(train: pl.DataFrame) -> TfidfVectorizer:
    """Train a TfidfVectorizer on the training data.

    Args:
        train (pl.DataFrame): Training data.

    Returns:
        TfidfVectorizer: Trained TfidfVectorizer.
    """
    tfidf_vectorizer = TfidfVectorizer(**vectorizer_params)
    tfidf_vectorizer.fit(train["corpus"].to_pandas().astype(str))
    return tfidf_vectorizer


def vectorize_data(data: pl.DataFrame, vectorizer: TfidfVectorizer) -> csr_matrix:
    """Transform text data into features using a TfidfVectorizer.

    Args:
        data (pl.DataFrame): Data to be vectorized.
        vectorizer (TfidfVectorizer): TfidfVectorizer to be used for vectorization.

    Returns:
        csr_matrix: Vectorized data.
    """
    features = vectorizer.transform(data["corpus"].to_pandas().astype(str))
    return features


def save_vectorizer(vectorizer: TfidfVectorizer, output_path: str) -> None:
    """Save TfidfVectorizer to file.

    Args:
        vectorizer (TfidfVectorizer): TfidfVectorizer to be saved.
        output_path (str): Path to save the TfidfVectorizer.
    """
    with open(output_path, "wb") as f:
        pickle.dump(vectorizer, f)


def save_vectorized_data_parquet(data: csr_matrix, polarity: csr_matrix, output_path: str) -> None:
    """Save vectorized data to parquet file.

    Args:
        data (csr_matrix): Vectorized data.
        polarity (csr_matrix): Polarity of the reviews.
        output_path (str): Path to save the vectorized data.
    """
    dense_features = data.toarray()
    df = pl.DataFrame(dense_features)
    df = df.with_columns([pl.Series(name="Polarity", values=polarity)])
    df.write_parquet(output_path)


def main() -> None:
    """Main function to vectorize Amazon reviews data."""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--input_path", type=str, default="data/processed/train.parquet")
    arg_parser.add_argument("--output_vectorizer", type=str, default="data/tfidf_vectorizer.pkl")
    arg_parser.add_argument("--output_train", type=str, default="data/features/train.parquet")
    arg_parser.add_argument("--output_test", type=str, default="data/features/test.parquet")
    args = arg_parser.parse_args()

    df = load_parquet_data(args.input_path)

    # Split data into train and test sets
    train, test = get_train_test_data(df, test_size=0.3)

    # Vectorize text data
    tfidf_vectorizer = train_vectorizer(train)
    train_features = vectorize_data(train, tfidf_vectorizer)
    test_features = vectorize_data(test, tfidf_vectorizer)

    # Save vectorizer to file
    save_vectorizer(tfidf_vectorizer, args.output_vectorizer)

    # Save vectorized data to parquet file
    save_vectorized_data_parquet(train_features, train["Polarity"].to_list(), args.output_train)
    save_vectorized_data_parquet(test_features, test["Polarity"].to_list(), args.output_test)


if __name__ == "__main__":
    main()
