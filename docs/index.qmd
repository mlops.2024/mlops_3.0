---
title: "Docs"
---

MlOps_3_0


## Workflow Management

[Snakemake](https://snakemake.readthedocs.io) is used to manage the workflow. Look at the `Snakefile` in the root directory for the workflow definition.

![DAG](dag.png){fig-alt="DAG"}

Tasks for the [iris dataset](https://scikit-learn.org/stable/auto_examples/datasets/plot_iris_dataset.html):

  1. `load_data` - loads the iris dataset and saves it to a CSV file.
  2. `preprocess` - preprocesses the data by scaling it (standard and min-max) and saving the results to CSV files to the path `data/processed`.
  3. `train_model` - trains a model with the preprocessed data and saves the model to the path `models`. Logistic regression and SVC are used.

In order to run the workflow, execute the following command:
```bash
snakemake --cores 4 -r -p
```
where `--cores` specifies the number of cores to use, `-r` prints the reason for each executed rule, and `-p` prints the shell command that will be executed.
For more information, refer to the [Snakemake documentation](https://snakemake.readthedocs.io/en/stable/).

---
Notebooks:

  - EDA: [NYC Tree Census 2015](notebooks/tree_census_dataset.ipynb)
