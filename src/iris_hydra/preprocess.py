"""Preprocess data: standard or minmax scaling."""

import logging

import hydra
import pandas as pd
from config_classes import MinMaxProcessing, StandardScalerProcessing
from omegaconf import DictConfig
from sklearn.preprocessing import MinMaxScaler, StandardScaler

log = logging.getLogger(__name__)


def read_raw_data(input_path: str) -> pd.DataFrame:
    """Read raw data.

    Args:
        input_path (str): Raw data input path

    Returns:
        pd.DataFrame: Raw data
    """
    return pd.read_csv(input_path)


def save_preprocessed_data(data: pd.DataFrame, output_path: str) -> None:
    """Save preprocessed data.

    Args:
        data (pd.DataFrame): Preprocessed data
        output_path (str): Preprocessed data output path
    """
    data.to_csv(output_path, index=False)


def standard_preprocess_data(data_features: pd.DataFrame, cfg: StandardScalerProcessing) -> pd.DataFrame:
    """Standard preprocessing.

    Args:
        data_features (pd.DataFrame): Features data
        cfg (StandardScalerProcessing): StandardScalerProcessing configuration

    Returns:
        pd.DataFrame: Preprocessed data
    """
    scaler = StandardScaler(with_mean=cfg.with_mean, with_std=cfg.with_std)
    features_scaled = scaler.fit_transform(data_features)
    features_scaled_df = pd.DataFrame(features_scaled, columns=data_features.columns)
    return features_scaled_df


def minmax_preprocess_data(data_features: pd.DataFrame, cfg: MinMaxProcessing) -> pd.DataFrame:
    """MinMax preprocessing.

    Args:
        data_features (pd.DataFrame): Features data
        cfg (MinMaxProcessing): MinMaxProcessing configuration

    Returns:
        pd.DataFrame: Preprocessed data
    """
    scaler = MinMaxScaler(feature_range=(cfg.feature_range["min"], cfg.feature_range["max"]))
    features_scaled = scaler.fit_transform(data_features)
    features_scaled_df = pd.DataFrame(features_scaled, columns=data_features.columns)
    return features_scaled_df


@hydra.main(config_path="conf", config_name="config", version_base=None)  # type: ignore
def main(cfg: DictConfig) -> None:
    """Main function for preprocessing data."""
    log.info(f"Configuration: {cfg.preprocessing}")

    # Prepare data
    data = read_raw_data(cfg.preprocessing.input_path)
    features = data.drop(columns=["species"])
    labels = data["species"]

    # Preprocess data
    if cfg.preprocessing.method == "standard":
        features_scaled_df = standard_preprocess_data(features, cfg.preprocessing)
    elif cfg.preprocessing.method == "minmax":
        features_scaled_df = minmax_preprocess_data(features, cfg.preprocessing)
    else:
        raise ValueError("Invalid method")

    # Save preprocessed data with labels
    features_scaled_df["species"] = labels
    log.info(f"Saving preprocessed data to {cfg.preprocessing.output_path}")
    save_preprocessed_data(features_scaled_df, cfg.preprocessing.output_path)


if __name__ == "__main__":
    main()
