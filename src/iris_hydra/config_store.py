"""Hydra ConfigStore for MLOps 3.0."""

from config_classes import (
    LogisticRegressionConfig,
    MainConfig,
    MinMaxProcessing,
    RawDataConfig,
    StandardScalerProcessing,
    SVMConfig,
)
from hydra.core.config_store import ConfigStore


def register_configs() -> None:
    """Register configs with Hydra ConfigStore."""
    cs = ConfigStore.instance()

    cs.store(name="config", node=MainConfig)
    cs.store(name="data", node=RawDataConfig)

    cs.store(group="preprocessing", name="standard", node=StandardScalerProcessing)
    cs.store(group="preprocessing", name="minmax", node=MinMaxProcessing)

    cs.store(group="model", name="logreg", node=LogisticRegressionConfig)
    cs.store(group="model", name="svm", node=SVMConfig)


register_configs()
