"""Configuration dataclasses."""

from dataclasses import dataclass, field
from typing import Dict


@dataclass
class ModelTrainConfig:
    """Dataclass for ModelTrainConfig."""

    input_path: str
    output_path: str
    model_type: str


@dataclass
class LogisticRegressionConfig(ModelTrainConfig):
    """Dataclass for LogisticRegressionConfig."""

    model_type: str = "logreg"
    penalty: str = "l2"
    C: float = 1.0


@dataclass
class SVMConfig(ModelTrainConfig):
    """Dataclass for SVMConfig."""

    model_type: str = "svm"
    kernel: str = "rbf"
    C: float = 1.0


@dataclass
class PreprocessConfig:
    """Dataclass for PreprocessConfig."""

    input_path: str
    output_path: str
    method: str


@dataclass
class MinMaxProcessing(PreprocessConfig):
    """Dataclass for MinMaxProcessing. Inherit from PreprocessConfig."""

    method: str = "minmax"
    feature_range: Dict[str, float] = field(default_factory=lambda: {"min": 0, "max": 1})


@dataclass
class StandardScalerProcessing(PreprocessConfig):
    """Dataclass for StandardScalerProcessing. Inherit from PreprocessConfig."""

    method: str = "standard"
    with_mean: bool = True
    with_std: bool = True


@dataclass
class RawDataConfig:
    """Dataclass for RawDataConfig."""

    input_path: str
    output_path: str


@dataclass
class MainConfig:
    """Dataclass for MainConfig."""

    raw: RawDataConfig
    preprocessing: PreprocessConfig
    model: ModelTrainConfig
