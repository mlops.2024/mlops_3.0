"""Tests for the preprocessing module."""

import re

from hypothesis import example, given
from hypothesis import strategies as st
from nltk.corpus import stopwords

from src.preprocessing import clean_text, clean_text_updated

stopwords_set = set(stopwords.words("english"))


@given(input_text=st.text())  # type: ignore
@example(input_text="123 Hello World!")  # type: ignore
@example(input_text="Visit https://example.com")  # type: ignore
@example(input_text="Non-English: Привет мир!")  # type: ignore
@example(input_text="Special chars: @#&*()!")  # type: ignore
@example(input_text="MiXeD CaSe WoRds")  # type: ignore
@example(input_text="Hello   World  ")  # type: ignore
def test_clean_text(input_text: str) -> None:
    """Test the clean_text function.

    Args:
        input_text (str): Input text to be cleaned.
    """
    result = clean_text(input_text)

    # Check that the result is lowercase
    assert result == result.lower()

    # Check that the result contains only lowercase letters and spaces
    assert re.match(r"^[a-z\s]*$", result)

    # check there are no double spaces
    assert "  " not in result

    # Check that the result does not contain stopwords
    for word in result.split():
        assert word not in stopwords_set

    # Check that the result is stripped
    assert result == result.strip()


@given(input_text=st.text())  # type: ignore
@example(input_text="123 Hello World!")  # type: ignore
@example(input_text="Visit https://example.com")  # type: ignore
@example(input_text="Non-English: Привет мир!")  # type: ignore
@example(input_text="Special chars: @#&*()!")  # type: ignore
@example(input_text="MiXeD CaSe WoRds")  # type: ignore
@example(input_text="Hello   World  ")  # type: ignore
def test_oracle_clean_text(input_text: str) -> None:
    """Test the clean_text function against clean_text_updated.

    Args:
        input_text (str): Input text to be cleaned.
    """
    current_results = [clean_text(s) for s in input_text]
    final_results = [clean_text_updated(s) for s in input_text]

    assert current_results == final_results
