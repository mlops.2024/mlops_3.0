ARG PYTHON_BASE=3.10-slim

FROM python:${PYTHON_BASE} AS builder
RUN pip install -U pdm
ENV PDM_CHECK_UPDATE=false
COPY pyproject.toml pdm.lock README.md /app/
RUN pdm install --check --prod --no-editable

FROM python:${PYTHON_BASE}
COPY --from=builder /app/.venv /app/.venv
ENV PATH=/app/.venv/bin:$PATH
COPY src /app/src
CMD ["python", "/app/src/mlops_3_0/main.py"]
