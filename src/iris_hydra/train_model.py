"""Train model and save it to disk."""

import logging
import pickle

import hydra
import pandas as pd
from config_classes import LogisticRegressionConfig, SVMConfig
from omegaconf import DictConfig
from pandas import Series
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

log = logging.getLogger(__name__)


def prepare_data(input_path: str) -> tuple[pd.DataFrame, Series[str]]:
    """Prepare data for training.

    Args:
        input_path (str): Input data path

    Returns:
        tuple[pd.DataFrame, Series[str]]: Features and target data
    """
    data = pd.read_csv(input_path)
    X = data.iloc[:, :-1]
    y = data["species"]
    return (X, y)


def train_logreg(X_train: pd.DataFrame, y_train: Series[str], cfg: LogisticRegressionConfig) -> LogisticRegression:
    """Train logistic regression model.

    Args:
        X_train (pd.DataFrame): Features data
        y_train (pd.Series[str][str]): Target data
        cfg (LogisticRegressionConfig): Logistic regression configuration

    Returns:
        LogisticRegression: Trained logistic regression model
    """
    model = LogisticRegression(penalty=cfg.penalty, C=cfg.C)
    model.fit(X_train, y_train)
    return model


def train_svm(X_train: pd.DataFrame, y_train: Series[str], cfg: SVMConfig) -> SVC:
    """Train SVM model.

    Args:
        X_train (pd.DataFrame): Features data
        y_train (pd.Series[str]): Target data
        cfg (SVMConfig): SVM configuration

    Returns:
        SVC: Trained SVM model
    """
    model = SVC(kernel=cfg.kernel, C=cfg.C)
    model.fit(X_train, y_train)
    return model


def save_pickle_model(model: object, output_path: str) -> None:
    """Save model to disk.

    Args:
        model: Trained model
        output_path (str): Output path
    """
    with open(output_path, "wb") as f:
        pickle.dump(model, f)


@hydra.main(config_path="conf", config_name="config", version_base=None)  # type: ignore
def main(cfg: DictConfig) -> None:
    """Main function for training model."""
    log.info(f"Training configuration: {cfg.model}")

    X_train, y_train = prepare_data(cfg.model.input_path)

    if cfg.model.type == "logreg":
        model = train_logreg(X_train, y_train, cfg.model)
    elif cfg.model.type == "svm":
        model = train_svm(X_train, y_train, cfg.model)
    else:
        raise ValueError("Invalid model type")

    save_pickle_model(model, cfg.model.output_path)


if __name__ == "__main__":
    main()
