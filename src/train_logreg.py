"""Train a logistic regression model on the features and labels."""

import argparse
import logging
import pickle
from typing import List

import polars as pl
from scipy.sparse import csr_matrix
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

random_state = 42

model_params = {"multi_class": "multinomial", "solver": "saga", "random_state": random_state}


def load_features_with_labels(data_path: str) -> tuple[csr_matrix, List[int]]:
    """Load features and labels from a parquet file.

    Args:
        data_path (str): Path to MatrixMarket file.

    Returns:
        tuple[csr_matrix, List[int]]
    """
    df = pl.read_parquet(data_path)
    polarity = df["Polarity"].to_numpy()
    feature_columns = df.columns
    feature_columns.remove("Polarity")
    features = csr_matrix(df[feature_columns].to_numpy())
    return features, polarity


def save_model(model: LogisticRegression, output_path: str) -> None:
    """Save a trained model to a file.

    Args:
        model (LogisticRegression): Trained model.
        output_path (str): Path to save the model.
    """
    with open(output_path, "wb") as model_file:
        pickle.dump(model, model_file)


def main() -> None:
    """Train a logistic regression model."""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--input_train_features", type=str, default="data/features/train.parquet")
    arg_parser.add_argument("--input_test_features", type=str, default="data/features/test.parquet")
    arg_parser.add_argument("--output_model", type=str, default="models/logreg_model.pkl")
    args = arg_parser.parse_args()

    # preview the input_train_features
    # df = pl.read_parquet(args.input_train_features, n_rows=3)
    # log.info(df)
    # return

    (train_features, train_labels) = load_features_with_labels(args.input_train_features)
    (test_features, test_labels) = load_features_with_labels(args.input_test_features)

    model_logreg = LogisticRegression(**model_params)
    model_logreg.fit(train_features, train_labels)

    predictions = model_logreg.predict(test_features)
    accuracy = accuracy_score(test_labels, predictions)
    log.info(f"Accuracy: {accuracy:.2f}")

    class_report = classification_report(test_labels, predictions)
    log.info(f"Classification report:\n{class_report}")

    conf_matrix = confusion_matrix(test_labels, predictions)
    log.info(f"Confusion matrix:\n{conf_matrix}")

    save_model(model_logreg, args.output_model)


if __name__ == "__main__":
    main()
