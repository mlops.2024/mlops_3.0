"""Read data from sklearn datasets and save it to csv."""

import logging

import hydra
import pandas as pd
from omegaconf import DictConfig
from sklearn import datasets

log = logging.getLogger(__name__)


def load_data() -> pd.DataFrame:
    """Load data (iris dataset).

    Returns:
        pd.DataFrame: Dataframe containing iris dataset
    """
    iris = datasets.load_iris()
    df = pd.DataFrame(iris.data, columns=iris.feature_names)
    df["species"] = [iris.target_names[label] for label in iris.target]
    return df


def save_data_to_csv(df: pd.DataFrame, output_path: str) -> None:
    """Save dataframe to CSV.

    Args:
        df (pd.DataFrame): Dataframe to save
        output_path (str): Output path
    """
    df.to_csv(output_path, index=False)


@hydra.main(config_path="conf", config_name="config", version_base=None)  # type: ignore
def main(cfg: DictConfig) -> None:
    """Main function for loading and saving data."""
    df = load_data()
    log.info(f"Saving data to {cfg.raw.output_path}")
    save_data_to_csv(df, cfg.raw.output_path)


if __name__ == "__main__":
    main()
